# Surf, the command line client for waives.io

surf allows you to quickly get started using [waives.io](https://waives.io). It can:
 * Perform OCR on documents and images, and generate searchable PDFs
 * Extract data from documents
 * Classify documents
 * Create custom extractors from modules
 * ...and more!

This repo also includes the waives.io client library for go, and is intended as a 'best-practice' integration with the [waives.io API](https://docs.waives.io/reference).

More documentation is available [here](https://docs.waives.io).